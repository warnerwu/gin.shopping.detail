package goods

import (
	"gin.shopping.detail/models"
	"github.com/gin-gonic/gin"
	"github.com/labstack/gommon/log"
	"net/http"
)

func GetDetail(c *gin.Context)  {
	id := c.Params.ByName("id")
	log.Printf("id value:%v\n", id)
	photos := models.GetPhotos()
	title := models.GetTitle()
	price := models.GetPrice()
	data := map[string]interface{}{"photos": photos, "title": title, "price": price}

	c.JSON(http.StatusOK, gin.H{"data": data, "message": "success", "status": http.StatusOK})
}

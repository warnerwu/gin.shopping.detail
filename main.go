package main

import (
	"gin.shopping.detail/api/goods"
	"github.com/gin-gonic/gin"
)

// Gin实现商品详情页
// 1. 搭建静态HTML服务
// 2. 实现AJAX - 服务器部分
// 3. 实现AJAX - HTML部分
func main() {
	// Gin应用程序实例
	engine := gin.Default()

	// 设定html文件夹路由, 用于存放index.html以及css/js等静态文件
	engine.Static("/html", "./html")
	engine.GET("/detail/:id", goods.GetDetail)

	// 启动服务
	engine.Run(":8081")
}
